<?php

namespace AtomBlueprint;

use AtomBlueprint\Renderers\BasicRenderer;

class Renderer extends BasicRenderer
{
    public function __construct(array $parsers)
    {
        $this->setParsers($parsers);
    }
}
