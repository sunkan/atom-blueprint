<?php

namespace AtomBlueprint\Utils;

class KeyLookup
{
    public static function lookup($key, array $data)
    {
        $info = explode('.', $key);
        if (count($info) > 1) {
            $obj = $info[0];
            $newKey = $info[1];
        }

        if (isset($newKey)) {
            return $data[$obj][$newKey] ?? false;
        }
        return $data[$key] ?? false;
    }
}
