<?php

namespace AtomBlueprint;

use AtomBlueprint\Components\ComponentInterface;

class Registry
{
    public function add(ComponentInterface $comp)
    {
        $this->comps[$comp->getName()] = $comp;
    }
    public function get($name)
    {
        return $this->comps[$name];
    }
}
