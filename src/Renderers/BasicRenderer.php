<?php

namespace AtomBlueprint\Renderers;

use AtomBlueprint\Components\ComponentInterface;
use AtomBlueprint\Parsers\AbstractParser;

class BasicRenderer implements RendererInterface
{
    protected $parsers;

    public function setParsers(array $parsers)
    {
        foreach ($parsers as $parser) {
            if ($parser instanceof AbstractParser) {
                $this->parsers[] = $parser;
            }
        }
    }
    public function getParsers()
    {
        return $this->parsers;
    }

    public function render(ComponentInterface $component, array $data)
    {
        $tpl = $component->getComponent();
        foreach ($this->parsers as $parser) {
            $subComponents = $parser->parse($tpl);
            if ($subComponents) {
                $replaceData = [];
                $renderer = $parser->getRenderer($this);
                foreach ($subComponents as $repl => $comp) {
                    $replaceData[$repl] = $renderer->render($comp, $data);
                }
                $tpl = str_replace(array_keys($replaceData), array_values($replaceData), $tpl);
            }
        }
        return $tpl;
    }
}
