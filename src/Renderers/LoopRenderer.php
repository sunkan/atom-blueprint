<?php

namespace AtomBlueprint\Renderers;

use AtomBlueprint\Components\ComponentInterface;
use AtomBlueprint\Components\LoopComponent;

class LoopRenderer extends BasicRenderer
{
    public function render(ComponentInterface $component, array $data)
    {
        if (!($component instanceof LoopComponent)) {
            return parent::render($component, $data);
        }
        $tpl = '';
        if ($component->getCollection($data)) {
            foreach ($component->getCollection($data) as $loopData) {
                $tpl .= parent::render($component, $component->populateData($loopData, $data));
            }
        }
        return $tpl;
    }
}
