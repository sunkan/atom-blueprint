<?php

namespace AtomBlueprint\Renderers;

use AtomBlueprint\Components\ComponentInterface;

interface RendererInterface
{
    public function render(ComponentInterface $component, array $data);
}
