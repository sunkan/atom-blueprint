<?php

namespace AtomBlueprint\Renderers;

use AtomBlueprint\Components\ComponentInterface;
use AtomBlueprint\Components\VariableComponent;

class VariableRenderer extends BasicRenderer
{
    public function render(ComponentInterface $component, array $data)
    {
        if (!($component instanceof VariableComponent)) {
            return parent::render($component, $data);
        }
        return $component->run($data);
    }
}
