<?php

namespace AtomBlueprint\Renderers;

use AtomBlueprint\Components\ComponentInterface;
use AtomBlueprint\Components\IfComponent;

class IfRenderer extends BasicRenderer
{
    public function render(ComponentInterface $component, array $data)
    {
        if (!($component instanceof IfComponent)) {
            return parent::render($component, $data);
        }
        if ($component->run($data)) {
            return parent::render($data);
        }
        return '';
    }
}
