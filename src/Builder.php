<?php

namespace AtomBlueprint;

class Builder
{
    protected $resolver;
    protected $registry;

    public function __construct(Registry $registry, callable $resolver)
    {
        $this->resolver = $resolver;
        $this->registry = $registry;
    }

    public function build(array $data)
    {
        $resolver = $this->resolver;
        $this->loopComponents($data['components']);
        return $this->body = $this->createComponent('body', $data['body']);
    }

    protected function createComponent($name, $html)
    {
        $resolver = $this->resolver;
        return $resolver('AtomBlueprint\Components\BasicComponent', [
            'name' => $name,
            'html' => $html,
        ]);
    }
    protected function loopComponents(array $components)
    {
        foreach ($components as $name => $component) {
            $this->registry->add($this->createComponent($name, $component['tpl']));
            if (isset($component['components']) && is_array($component['components'])) {
                $this->loopComponents($component['components']);
            }
        }
    }
}
