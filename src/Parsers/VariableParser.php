<?php

namespace AtomBlueprint\Parsers;

use AtomBlueprint\Renderers\RendererInterface;

class VariableParser extends AbstractParser
{
    public function parse($block)
    {
        return $this->match('/{([a-z\.]+)}/i', $block, function ($info) {
            return $this->factory->createVariable($info[1]);
        });
    }

    public function getRenderer(RendererInterface $baseRenderer): RendererInterface
    {
        $renderer = $this->factory->createVariableRenderer();
        $renderer->setParsers($baseRenderer->getParsers());
        return $renderer;
    }
}
