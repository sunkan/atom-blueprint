<?php

namespace AtomBlueprint\Parsers;

use AtomBlueprint\Renderers\RendererInterface;

class LoopParser extends AbstractParser
{
    public function parse($block)
    {
        return $this->match('/{([\w\.]+)\ as\ (\w+)}(.*){\/\1}/is', $block, function ($loopInfo) {
            return $this->factory->createLoop($loopInfo[3], $loopInfo[1], $loopInfo[2]);
        });
    }

    public function getRenderer(RendererInterface $baseRenderer): RendererInterface
    {
        $renderer = $this->factory->createLoopRenderer();
        $renderer->setParsers($baseRenderer->getParsers());
        return $renderer;
    }
}
