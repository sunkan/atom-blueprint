<?php

namespace AtomBlueprint\Parsers;

use AtomBlueprint\Renderers\RendererInterface;

class CallbackParser extends AbstractParser
{
    public function parse($block)
    {
        return $this->match('/{([\w\.]+)\((.*)\)}/i', $block, function ($clbInfo) {
            return $this->factory->createCallback($clbInfo[1], $clbInfo[2]);
        });
    }
    public function getRenderer(RendererInterface $baseRenderer): RendererInterface
    {
        $renderer = $this->factory->createVariableRenderer();
        $renderer->setParsers($baseRenderer->getParsers());
        return $renderer;
    }
}
