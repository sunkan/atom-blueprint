<?php

namespace AtomBlueprint\Parsers;

use AtomBlueprint\Factory;
use AtomBlueprint\Renderers\RendererInterface;

abstract class AbstractParser
{
    protected $factory;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }
    protected function match($reg, $block, $clb)
    {
        $rs = preg_match_all($reg, $block, $matches, \PREG_SET_ORDER);
        if (!$rs) {
            return false;
        }

        $returnData = [];
        foreach ($matches as $info) {
            $returnData[$info[0]] = $clb($info);
        }
        return $returnData;
    }

    abstract public function parse($block);

    public function getRenderer(RendererInterface $baseRenderer): RendererInterface
    {
        return $baseRenderer;
    }
}
