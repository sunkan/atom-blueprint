<?php

namespace AtomBlueprint\Parsers;

use AtomBlueprint\Renderers\RendererInterface;

class IfParser extends AbstractParser
{
    public function parse($block)
    {
        return $this->match('/{if\s+?\(([\w\.\!]+)\)}(.*){\/if}/is', $block, function ($ifInfo) {
            return $this->factory->createIf($ifInfo[2], $ifInfo[1]);
        });
    }
    public function getRenderer(RendererInterface $baseRenderer): RendererInterface
    {
        $renderer = $this->factory->createIfRenderer();
        $renderer->setParsers($baseRenderer->getParsers());
        return $renderer;
    }
}
