<?php

namespace AtomBlueprint\Parsers;

class ComponentParser extends AbstractParser
{
    public function parse($block)
    {
        return $this->match('/{component\:(\w+)(\((\w+)\))?}/i', $block, function ($compInfo) {
            $comp = $this->factory->getComponent($compInfo[1]);
            $comp->setContext($compInfo);
            return $comp;
        });
    }
}
