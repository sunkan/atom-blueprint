<?php

namespace AtomBlueprint\Components;

class CallbackComponent extends VariableComponent
{
    protected $args;

    public function __construct($name, $html, $key, $args)
    {
        parent::__construct($name, $html, $key);
        $this->args = explode(',', $args);
    }

    public function run(array $data)
    {
        $clb = parent::run($data);

        if (is_callable($clb)) {
            return $clb(...$this->args);
        }
        return $clb;
    }
}
