<?php

namespace AtomBlueprint\Components;

interface ComponentInterface
{
    public function getName();
    public function getComponent();
}
