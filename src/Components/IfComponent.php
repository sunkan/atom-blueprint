<?php

namespace AtomBlueprint\Components;

use AtomBlueprint\Utils\KeyLookup;

class IfComponent extends AbstractComponent
{
    protected $key;

    public function __construct($name, $info, $expr)
    {
        parent::__construct($name, $info);
        $this->bool = $expr[0] === '!' ? false : true;
        $this->key = !$this->bool ? substr($expr, 1) : $expr;
    }

    public function run(array $data)
    {
        $value = KeyLookup::lookup($this->key, $data);
        if ($this->bool) {
            return (bool) $value;
        }
        return !((bool) $value);
    }
}
