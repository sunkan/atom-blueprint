<?php

namespace AtomBlueprint\Components;

class BasicComponent extends AbstractComponent
{
    private $contextInfo;

    public function setContext(array $info)
    {
        $this->contextInfo = $info;
    }
}
