<?php

namespace AtomBlueprint\Components;

use AtomBlueprint\Helper\HelperInterface;
use AtomBlueprint\Utils\KeyLookup;

class HelperComponent extends VariableComponent
{
    protected $key;
    protected $args;

    public function __construct($name, $html, HelperInterface $helper, $args)
    {
        $this->helper = $helper;
        $this->args = array_map('trim', explode(',', $args));
    }

    public function run(array $data)
    {
        if ($this->helper) {
            $value = KeyLookup::lookup($this->args[0], $data);
            if ($value) {
                $this->args[0] = $value;
            }
            return $this->helper->run($this->args, $data);
        }
        return '';
    }
}
