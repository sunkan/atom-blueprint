<?php

namespace AtomBlueprint\Components;

abstract class AbstractComponent implements ComponentInterface
{
    protected $name;
    protected $template;

    public function __construct($name, $html)
    {
        $this->name = $name;
        $this->template = $html;
    }
    public function getName()
    {
        return $this->name;
    }

    public function getComponent()
    {
        return $this->template;
    }
}
