<?php

namespace AtomBlueprint\Components;

use AtomBlueprint\Utils\KeyLookup;

class VariableComponent extends AbstractComponent
{
    protected $key;
    protected $args;

    public function __construct($name, $html, $key)
    {
        parent::__construct($name, $html);
        $this->key = $key;
    }

    public function run(array $data)
    {
        return KeyLookup::lookup($this->key, $data);
    }
}
