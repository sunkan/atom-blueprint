<?php

namespace AtomBlueprint\Components;

use AtomBlueprint\Utils\KeyLookup;

class LoopComponent extends AbstractComponent
{
    protected $collectionInfo;

    public function __construct($name, $info, array $collectionInfo)
    {
        parent::__construct($name, $info);
        $this->collectionInfo = $collectionInfo;
    }

    public function getCollection($data)
    {
        return KeyLookup::lookup($this->collectionInfo['collection'], $data);
    }

    public function populateData($loopData, array $data)
    {
        $newArray = $data;
        $newArray[$this->collectionInfo['key']] = $loopData;

        return $newArray;
    }
}
