<?php

namespace AtomBlueprint;

class Factory
{
    private $guid = 1;
    public function __construct(Registry $registry, Helper\ResolverInterface $helperResolver, callable $resolver)
    {
        $this->resolver = $resolver;
        $this->registry = $registry;
        $this->helperResolver = $helperResolver;
    }

    protected function create($cls, array $args)
    {
        $resolver = $this->resolver;
        return $resolver($cls, $args);
    }

    public function createVariableRenderer()
    {
        return new Renderers\VariableRenderer();
    }

    public function createLoopRenderer()
    {
        return new Renderers\LoopRenderer();
    }

    public function createIfRenderer()
    {
        return new Renderers\IfRenderer();
    }

    public function createCallback($key, $args)
    {
        $this->guid++;
        return new Components\CallbackComponent('clb_' . $this->guid, '', $key, $args);
    }

    public function createHelper($helper, $args)
    {
        $this->guid++;
        $helperClb = $this->helperResolver->resolve($helper);
        return new Components\HelperComponent('helper_' . $this->guid, '', $helperClb, $args);
    }

    public function createIf($html, $expr)
    {
        $this->guid++;
        return new Components\IfComponent('if_' . $this->guid, $html, $expr);
    }
    public function createLoop($html, $collection, $loopKey)
    {
        $this->guid++;
        return new Components\LoopComponent('loop_' . $this->guid, $html, [
            'collection' => $collection,
            'key' => $loopKey,
        ]);
    }

    public function createVariable($key)
    {
        $this->guid++;
        return new Components\VariableComponent('key_' . $this->guid, '', $key);
    }

    public function getComponent($name)
    {
        $comp = $this->registry->get($name);
        if ($comp instanceof Components\ComponentInterface) {
            return clone $comp;
        }
        throw new Exception('Unkown component: ' . $name);
    }
}
