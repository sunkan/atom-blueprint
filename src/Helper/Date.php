<?php
namespace AtomBlueprint\Helper;

use DateTime;
use DateTimeZone;

class Date implements HelperInterface
{
    public function getName()
    {
        return 'date';
    }

    public function run($args, array $payload)
    {
        $argc = count($args);
        if ($argc == 0) {
            return $this;
        }

        $date = $args[0];

        if ($date == '0000-00-00 00:00:00') {
            return 'Not a valid date';
        }
        return $this->date($date, $args[1], ($args[2] ? $args[2] : true), ($args[3] ? $args[3] : false));
    }

    protected $defaultTimeZone = 'Europe/Stockholm';
    protected $timezone = null;

    public function setTimeZone($tz)
    {
        try {
            $this->timezone = new \DateTimeZone($tz);
        } catch (\Exception $e) {
            return;
        }
    }

    public function getTimestamp($date)
    {
        if (!($date instanceof DateTime)) {
            $date = new DateTime((is_int($date) ? '@' . $date : $date));
        }
        if (!$this->timezone) {
            $this->setTimeZone($this->defaultTimeZone);
        }
        $date->setTimezone($this->timezone);

        return $date->format('U');
    }

    public function format($date, array $info = array())
    {
        if (!$this->timezone) {
            $this->setTimeZone($this->defaultTimeZone);
        }
        $dt = new \DateTime((is_int($date) ? '@' . $date : $date));
        $dt->setTimezone($this->timezone);

        $date = $dt->format('Y-m-d');
        $today = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime('-1 day'));
        $week = strtotime('-5 day');
        $lastYear = strtotime('-330 day');

        $str = '';

        $timestamp = $this->getTimestamp($dt);
        if ($date == $today) {
            $str .= 'Idag' . ' ';
        } elseif ($date == $yesterday) {
            $str .= 'Igår' . ' ';
        } elseif ($week < $timestamp) {
            $str .= ucfirst(strftime('%As ', $timestamp));
        } else {
            $str .= strftime('%e %B', $timestamp);
            if ($lastYear > $timestamp) {
                $str .= $dt->format(', Y');
            }
        }
        $str .= $dt->format(' - H:i');

        return ($str);
    }
    public function date($date, $format = null, $show_time = true, $strftime = false)
    {
        if (!$this->timezone) {
            $this->setTimeZone($this->defaultTimeZone);
        }
        $dt = new DateTime((is_int($date) ? '@' . $date : $date));
        $dt->setTimezone($this->timezone);

        if ($format != null && $format != false) {
            if ($strftime) {
                $timestamp = $this->getTimestamp($dt);
                return strftime($format, $timestamp);
            }
            if ($format == 'U') {
                return $this->getTimestamp($dt);
            }
            return $dt->format($format);
        }
        $date = $dt->format('Y-m-d');
        $today = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime('-1 day'));
        $week = strtotime('-5 day');

        $str = '';

        $timestamp = $this->getTimestamp($dt);
        if ($date == $today) {
            $str .= 'Idag ';
        } elseif ($date == $yesterday) {
            $str .= 'Igår ';
        } elseif ($week < $timestamp) {
            $str .= utf8_encode(ucfirst(trim(strftime('%A', $timestamp)) . 's '));
        } else {
            $str .= utf8_encode(strftime('%e %B, %Y ', $timestamp));
        }

        if ($show_time) {
            $str .= $dt->format('H:i');
        }

        return ($str);
    }
}
