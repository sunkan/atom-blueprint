<?php

namespace AtomBlueprint\Helper;

interface ResolverInterface
{
    public function resolve($method);
}
