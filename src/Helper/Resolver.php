<?php

namespace AtomBlueprint\Helper;

use Aura\Di\Container as Di;

class Resolver implements ResolverInterface
{
    protected $map = [];
    protected $ns = [];

    public function __construct(Di $di)
    {
        $this->di = $di;
    }

    public function setNs(array $ns)
    {
        $this->ns = $ns;
    }

    public function addNs($ns)
    {
        $this->ns[] = $ns;
    }

    public function addClass(IHelper $class)
    {
        $this->map[$class->getName()] = $class;
    }

    public function addFunction($key, $func)
    {
        $this->map[$key] = $func;
    }

    public function resolve($method)
    {
        if (isset($this->map[$method])) {
            return $this->map[$method];
        }
        $found = false;
        foreach (array_reverse($this->ns) as $ns) {
            $className = $ns . '\\' . ucfirst($method);
            if (class_exists($className)) {
                $helper = $this->di->newInstance($className);
                $found = true;
                break;
            }
        }
        if (!$found) {
            $this->map[$method] = false;
            return false;
        }
        return $this->map[$method] = $helper;
    }
}
