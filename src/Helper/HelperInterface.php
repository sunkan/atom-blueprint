<?php
namespace AtomBlueprint\Helper;

interface HelperInterface
{
    public function getName();
    public function run($args, array $payload);
}
