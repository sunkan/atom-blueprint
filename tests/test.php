<?php
include '../vendor/autoload.php';

use Aura\Di\Container;
use Aura\Di\Factory;

$di = new Container(new Factory);

$di->params['AtomBlueprint\Helper\Resolver']['di'] = $di;
$di->setter['AtomBlueprint\Helper\Resolver']['setNs'] = ['AtomBlueprint\Helper'];

$di->set('atom_registry', $di->lazyNew('AtomBlueprint\Registry'));
$di->set('atom_factory', $di->lazyNew('AtomBlueprint\Factory'));

$resolver = function ($cls, $args) use ($di) {
    return $di->newInstance($cls, $args);
};

$di->params['AtomBlueprint\Factory'] = [
    'registry' => $di->lazyGet('atom_registry'),
    'helperResolver' => $di->lazyNew('AtomBlueprint\Helper\Resolver'),
    'resolver' => $resolver,
];
$di->params['AtomBlueprint\Builder'] = [
    'registry' => $di->lazyGet('atom_registry'),
    'resolver' => $resolver,
];
$di->params['AtomBlueprint\Parsers\AbstractParser']['factory'] = $di->lazyGet('atom_factory');

$di->set('render_parsers', function () use ($di) {
    return [
        $di->newInstance('AtomBlueprint\Parsers\IfParser'),
        $di->newInstance('AtomBlueprint\Parsers\LoopParser'),
        $di->newInstance('AtomBlueprint\Parsers\ComponentParser'),
        $di->newInstance('AtomBlueprint\Parsers\HelperParser'),
        $di->newInstance('AtomBlueprint\Parsers\CallbackParser'),
        $di->newInstance('AtomBlueprint\Parsers\VariableParser'),
    ];
});

$di->params['AtomBlueprint\Renderer']['parsers'] = $di->lazyGet('render_parsers');

$designData = json_decode('{
    "_id" : "",
    "blog_id" : 860,
    "body" : "<html><head><title>{blog.title}</title></head><body>{component:header}{component:list}</body></html>",
    "components" : {
        "header" : {
            "tpl" : "<header><h1>{blog.title}</h1></header>"
        },
        "list" : {
            "tpl" : "<div class=\"posts\">\n{posts as post}\n{component:post(post)}\n{/posts}\n</div>\n{if (blog.paging)}\n<div class=\"paging\">\n  {pager.previus_url(<a href=\"{url}\">Föregående sida</a>)}\n  <a href=\"#top\">Topp</a>\n  {pager.next_url(<a href=\"{url}\">Nästa sida</a>)}\n</div>\n{/if}",
            "components" : {
                "post" : {
                    "tpl" : "<article>\n<h2><a href=\"{post.url}\">{post.title}</a></h2>\n<div>\n  {post.content}\n</div>\n<footer>\n  <time>{f:date(post.date)}</time>\n  {post.categories as cat}\n      <a href=\"{cat.url}\" title=\"{cat.name}\">{cat.name}</a>\n  {/post.categories}\n  {post.comment_link(<a href=\"{url}\">Kommentarer</a>)}\n</footer>\n</article>\n"
                }
            }
        }
    }
}', true);

$builder = $di->newInstance('AtomBlueprint\Builder');
$renderer = $di->newInstance('AtomBlueprint\Renderer');

$comment_link = function ($html) {
    return $html;
};
var_dump($renderer->render($builder->build($designData), [
    'blog' => [
        'title' => 'test blog',
        'url' => 'sunkan.se',
    ],
    'posts' => [
        [
            'date' => '2016-09-06T08:22:23',
            'title' => 'test1',
            'content' => '<p>Test content</p>',
            "comment_link" => $comment_link,
            'categories' => [
                [
                    'name' => 'Allmänt',
                ], [
                    'name' => 'Test',
                ],
            ],
        ],
        [
            'date' => '2016-05-21T21:21:22',
            'title' => 'test2',
            'content' => '<p>Test content</p>',
            "comment_link" => $comment_link,
            'categories' => [
                ['name' => 'Allmänt'],
            ],
        ],
    ],
]));
